# xd
### 目次
* [基本](https://gitlab.com/alt-b/xd/-/blob/master/xd_memo.md)
* [事例](https://gitlab.com/alt-b/xd/-/blob/master/casestudy.md)
* [応用（実践使える）](https://gitlab.com/alt-b/xd/-/blob/master/practice.md)
* [困った事（あるある）](https://gitlab.com/alt-b/xd/-/blob/master/commonthings.md)
* [最新情報](https://gitlab.com/alt-b/xd/-/blob/master/latestinformation.md)
