# 応用

## 便利なショートカット
* コピペ
  * 同じサイズのアートボードでコピペすると、同じ場所にコピーできる
  * アートボードごとコピペできる
* アセット追加
  * オブジェクト選択してctrl（comand）+xxでアセットにカラー追加
  * オブジェクト選択してctrl（comand）+xxでアセットにフォント追加
  * オブジェクト選択してctrl（comand）+xxでアセットにコンポーネント追加

## 拡大縮小
* オブジェクトの比率を保って拡大縮小
  * イラストなどのベクターをグループ化している場合、数値でサイズ変更するとうまくサイズ変更できなくイラストが崩れてしまう
  * バウンディングボックスでshift押しながらサイズ変更するとうまくいくが、サイズの数値を整数にするのが難しい
  * 角丸などのオブジェクトに設定したものは、サイズ変更しても数値が変更されないので注意
* 幅に応じて自動サイズ変更
  * WEBのレスポンシブデザインのようにグループ化したオブジェクトの幅を変更するとよしなに伸びてくれる
  * パーツの作り方や設定によってはうまく伸びてくれないことがある
* テキストのサイズ変更
  * ボックスの設定によってサイズ変更に影響する
    * テキストボックス？の場合は文字サイズも変更される。比率を保って拡大縮小する
    * フローテキスト？の場合はボックスのサイズが変更されるだけでテキストサイズは変わらない
  * 

## テキスト
* 
  * 

## コンポーネント
* 
  * 

## ステート設定
* ボタン
  * forcus、default、press、desableなどのステータスを設定できる
* 
  * 

## モックの設定
* 動きの設定
  * トリガー
    * タップ
    * 音声
  * アニメーション
    * 画面左右スライド
    * 画面上下スライド
  * 
    * 

* プレビュー
  * 画面右上の再生ボタン（ショートカット：？+？+？）

## 書き出し・保存
* 書き出し
  * 個別に書き出し
  * まとめて書き出し
* 保存
  * ローカルに保存
  * クラウド保存

## URL発行
* URL発行について
  * URL発行する時にいろいろなモードを設定できる。デザインレビュー用、指示書用（開発モード）、カスタムモードがある
  * URL閲覧時にパスワードを入力するように設定できる
  * 一度発行すると更新はボタンタップのみでOK
  * モードは後から変更できるが、パスワード設定は後から変更できないので注意。パスワード設定したい場合はURLが変更になってしまう
  * XDファイルをコピペしたりファイル名を変更してもURL発行履歴は残る。但し、違うユーザーではURL更新できない
* モード設定
  * デザインレビュー
    * シート閲覧、コメント機能が使える
  * 開発モード
    * シート閲覧、コメント機能、指示書として使える（文字サイズ、色の数値、オブジェクトのサイズや間隔など）
    * web、iOS、Androidを選べて、それぞれで単位が違うようになる
